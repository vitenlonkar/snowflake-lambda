package com.graytrails.lambda.test;

import com.amazonaws.services.lambda.runtime.Context;
import com.graytrails.lambda.testutils.TestContext;
import com.graytrails.snowflake.lambda.KNMIStagingUtilityFunctionHandler;
import org.junit.Test;


public class KNMIStagingUtilityFunctionHandlerTest {

    private Context createContext() {
        TestContext ctx = new TestContext();
        ctx.setFunctionName("handleRequest");
        return ctx;
    }

    @Test
    public void testLambdaFunctionHandler() {
        KNMIStagingUtilityFunctionHandler handler = new KNMIStagingUtilityFunctionHandler();
        Context ctx = createContext();
        handler.handleRequest("Test Lambda", ctx);
    }

}
