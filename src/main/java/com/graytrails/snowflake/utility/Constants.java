package com.graytrails.snowflake.utility;

public interface Constants {

    String URL_START = "https://monitoringapi.solaredge.com/equipment/";
    String PLANT_ID_HEADER = "plantId";
    String INVERTER_ID_HEADER = "inverterId";
    String KNMI_URL = "http://projects.knmi.nl/klimatologie/uurgegevens/getdata_uur.cgi" ;
}
