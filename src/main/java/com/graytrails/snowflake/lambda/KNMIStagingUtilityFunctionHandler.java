package com.graytrails.snowflake.lambda;

import com.amazonaws.services.lambda.runtime.Context;
import com.amazonaws.services.lambda.runtime.RequestHandler;
import com.amazonaws.services.s3.AmazonS3;
import com.amazonaws.services.s3.AmazonS3ClientBuilder;
import com.amazonaws.services.s3.model.PutObjectRequest;
import com.graytrails.snowflake.utility.Constants;
import org.springframework.http.*;

import javax.ws.rs.client.Client;
import javax.ws.rs.client.ClientBuilder;
import javax.ws.rs.client.WebTarget;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.net.URLDecoder;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.time.ZonedDateTime;
import java.time.format.DateTimeFormatter;
import java.util.Calendar;

public class KNMIStagingUtilityFunctionHandler implements RequestHandler<String, Void> {

    public KNMIStagingUtilityFunctionHandler() {

    }

    AmazonS3 amazonS3 = AmazonS3ClientBuilder.standard().build();
    Client client = ClientBuilder.newClient();


    public Void handleRequest(String input, Context context) {
        context.getLogger().log("Started lambda function to create and dump KNMI files in s3");


        //Date and Time using JAVA8
        ZoneId zoneId = ZoneId.of("Europe/Amsterdam");
        LocalDate localDate = LocalDate.now(zoneId);
        String previousDay = localDate.minusDays(1).format(DateTimeFormatter.ofPattern("yyyyMMdd"));
        context.getLogger().log(previousDay);

        try {
            callAPIAndCreateTempFileAndDumpToS3KNMIStage(previousDay, context);
        } catch (Exception e) {
            e.printStackTrace();
        }

        //copyFileToS3(prevDate);
        //triggerSql();


        return null;
    }

    private void callAPIAndCreateTempFileAndDumpToS3KNMIStage( String prevDate, Context context)  {
        context.getLogger().log("inside callAPIAndCreateTempFileAndDumpToS3KNMIStage");
        String transactionUrl = Constants.KNMI_URL;
        byte[] returnResult = client.target(transactionUrl)
                .queryParam("vars", "Q")
                .request()
                .get(byte[].class);
        System.out.println(returnResult);

        try {
            Files.write(Paths.get("/tmp/"+prevDate+"_hourly.csv"), returnResult);
            amazonS3.putObject(System.getenv("BUCKET_NAME"), prevDate+"_hourly.csv",  new File("/tmp/"+prevDate+"_hourly.csv"));

        } catch (IOException e) {
            e.printStackTrace();
        }

    }

   

    private void clearTempFile(String path) {
        File file = new File(path);
        file.delete();
    }
}
